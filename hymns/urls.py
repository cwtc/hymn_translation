from django.urls import path

from . import views

urlpatterns = [
	path('home', views.home, name='home'),
    path('index', views.index, name='index'),
    path('subjects', views.subjects, name='subjects'),
    path('countries', views.countries, name='countries')
]