from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render

from .models import Hymn, Author, Composer, Country, Subject


def home(request):
	context = {}
	return render(request, 'hymns/home.html', context)

def index(request):
	context = {}
	return render(request, 'hymns/index.html', context)

def subjects(request):
	subjects = []
	hymns_by_subject = {}
	for subject in Subject.objects.all():
		subjects.append(str(subject))
		hymns = Hymn.objects.all().filter(subject=subject)
		hymns_by_subject[str(subject.subject)] = hymns
	context = {
		'subjects': subjects,
		'hymns_by_subject': hymns_by_subject,
		}
	return render(request, 'hymns/subjects.html', context)

def countries(request):
	countries = []
	hymns_by_country = {}
	for country in Country.objects.all():
		countries.append(str(country))
		hymns = Hymn.objects.all().filter(country=country)
		hymns_by_country[str(country.country)] = hymns
	context = {
		'countries': countries,
		'hymns_by_country': hymns_by_country,
		}
	return render(request, 'hymns/countries.html', context)