from django.contrib import admin

# Register your models here.

from .models import Hymn, Author, Composer, Subject, Country
from import_export import resources
from import_export.admin import ImportExportModelAdmin

"""admin.site.register(Hymn)
admin.site.register(Author)
admin.site.register(Composer)
admin.site.register(Subject)
admin.site.register(Country)"""

class HymnResource(resources.ModelResource):
	class Meta:
		model = Hymn

class AuthorResource(resources.ModelResource):
	class Meta:
		model = Author

class ComposerResource(resources.ModelResource):
	class Meta:
		model = Composer

class SubjectResource(resources.ModelResource):
	class Meta:
		model = Subject

class CountryResource(resources.ModelResource):
	class Meta:
		model = Country

@admin.register(Hymn)
class HymnAdmin(ImportExportModelAdmin):
	resource_class = HymnResource

@admin.register(Author)
class AuthorAdmin(ImportExportModelAdmin):
	resource_class = AuthorResource

@admin.register(Composer)
class ComposerAdmin(ImportExportModelAdmin):
	resource_class = ComposerResource

@admin.register(Subject)
class SubjectAdmin(ImportExportModelAdmin):
	resource_class = SubjectResource	

@admin.register(Country)
class CountryAdmin(ImportExportModelAdmin):
	resource_class = CountryResource