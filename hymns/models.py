from django.db import models

# Create your models here.

class Author(models.Model):
	author_last_name = models.CharField('Author first name', max_length=200)
	author_first_name = models.CharField('Author last name', max_length=200)
	author_birth = models.CharField('Author birth', max_length=200)
	author_death = models.CharField('Author death', max_length=200)

	def __str__(self):
		return '{} {}'.format(self.author_first_name, self.author_last_name)

class Composer(models.Model):
	composer_last_name = models.CharField('Composer first name', max_length=200)
	composer_first_name = models.CharField('Composer last name', max_length=200)
	composer_birth = models.CharField('Composer birth', max_length=200)
	composer_death = models.CharField('Composer death', max_length=200)

	def __str__(self):
		return '{} {}'.format(self.composer_first_name, self.composer_last_name)

class Subject(models.Model):
	subject = models.CharField('Subject', max_length=200)

	def __str__(self):
		return self.subject

class Country(models.Model):
	country= models.CharField('Country name', max_length=200)

	def __str__(self):
		return self.country

class Hymn(models.Model):
	tune_name = models.CharField('Tune name', max_length=200)
	en_title = models.CharField('English title', max_length=200)

	author = models.ForeignKey(Author, on_delete=models.CASCADE)
	composer = models.ForeignKey(Composer, on_delete=models.CASCADE)
	subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
	country = models.ForeignKey(Country, on_delete=models.CASCADE)

	def __str__(self):
		return '{}, ({})'.format(self.tune_name, self.en_title)