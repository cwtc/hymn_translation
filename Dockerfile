# Pull base image
FROM python:3.7

# Set environment varibles
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Set work directory
WORKDIR /code

# Install dependencies
RUN pip install --upgrade pip
RUN pip install pipenv
#COPY ./Pipfile /code/Pipfile
#RUN pipenv install --deploy --system --skip-lock --dev
#RUN pipenv install psycopg2
RUN pip install psycopg2 django django-import-export django-bootstrap3 django-angular

# Copy project
COPY . /code/
